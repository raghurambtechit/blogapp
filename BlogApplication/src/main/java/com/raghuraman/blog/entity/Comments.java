package com.raghuraman.blog.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comments")

public class Comments {

	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "comment_id")
	private Integer comment_id;

	public Integer getComment_id() {
		return comment_id;
	}

	public void setComment_id(Integer comment_id) {
		this.comment_id = comment_id;
	}

	@Column(name = "username")
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "comment")
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	@ManyToOne(optional = false)
	@JoinColumn(name = "blog_id")
	private Blog blogForComment;

	public Blog getBlogForComment() {
		return blogForComment;
	}

	public void setBlogForComment(Blog blogForComment) {
		this.blogForComment = blogForComment;
	}

	public Comments() {

	}

	public Comments(String username, String comment) {

		this.username = username;
		this.comment = comment;
	}

}
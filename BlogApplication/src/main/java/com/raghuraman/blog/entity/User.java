package com.raghuraman.blog.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	/**
	 * 
	 */
	// private static final long serialVersionUID = -7297944049508146483L;

	public User(String password, Integer enabled) {

		this.password = password;
		this.enabled = enabled;
	}

	@Id
	@Column(name = "user_id")

	private Integer userId;

	public Integer getUser_id() {
		return userId;
	}

	public void setUser_id(Integer user_id) {
		this.userId = user_id;
	}

	@Column(name = "username")
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	// @OneToMany(mappedBy = "user1", cascade = {CascadeType.REMOVE})
	// private List<Friends> user1;
	//
	//
	// @OneToMany(mappedBy = "user2", cascade = {CascadeType.REMOVE})
	// private List<Friends> user2;

	// public List<Friends> getUser1() {
	// return user1;
	// }
	//
	// public void setUser1(List<Friends> user1) {
	// this.user1 = user1;
	// }
	//
	// public List<Friends> getUser2() {
	// return user2;
	// }
	//
	// public void setUser2(List<Friends> user2) {
	// this.user2 = user2;
	// }

	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
	private Authorities authorities;

	public Authorities getAuthorities() {
		return authorities;
	}

	public void setAuthorities(Authorities authorities) {
		this.authorities = authorities;
	}

	@OneToMany(mappedBy = "user", targetEntity = Blog.class, cascade = { CascadeType.REMOVE })
	private List blogs = new ArrayList();

	public List getBlogs() {
		return blogs;
	}

	public void setBlogs(List blogs) {
		this.blogs = blogs;
	}

	@Column(name = "password")
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "enabled")
	private Integer enabled;

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

	public User() {
	}

}
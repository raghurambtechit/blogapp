package com.raghuraman.blog.DTO;

public class PasswordDTO {
	public String password;
	public String newPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public PasswordDTO(String password, String newPassword) {
		super();
		this.password = password;
		this.newPassword = newPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public PasswordDTO() {
		super();

	}

}

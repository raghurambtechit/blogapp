package com.raghuraman.blog.DTO;

public class UserDTO {

	private String username;
	private String password;
	private Integer user_id;
	private Integer enabled;
	private String newUsername;

	public String getNewUsername() {
		return newUsername;
	}

	public void setNewUsername(String newUsername) {
		this.newUsername = newUsername;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserDTO() {
		super();
	}

	public UserDTO(String username, String password, Integer enabled, Integer user_id) {

		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.user_id = user_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public Integer getEnabled() {
		return enabled;
	}

	public void setEnabled(Integer enabled) {
		this.enabled = enabled;
	}

}

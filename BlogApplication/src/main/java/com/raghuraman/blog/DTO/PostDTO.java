package com.raghuraman.blog.DTO;

public class PostDTO {

	private String username;
	private String blog;
	private Integer post_id;
	private Integer user_id;
	private Integer like_id;
	private Integer comment_id;
	// private long liked_users;

	public String getUsername() {
		return username;
	}

	public Integer getLike_id() {
		return like_id;
	}

	public void setLike_id(Integer like_id) {
		this.like_id = like_id;
	}

	public Integer getComment_id() {
		return comment_id;
	}

	public void setComment_id(Integer comment_id) {
		this.comment_id = comment_id;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getBlog() {
		return blog;
	}

	public void setBlog(String blog) {
		this.blog = blog;
	}

	public Integer getpost_id() {
		return post_id;
	}

	public void setpost_id(Integer blog_id) {
		this.post_id = blog_id;
	}
//	public long getLiked_users() {
//		return liked_users;
//	}
//	public void setLiked_users(long liked_users) {
//		this.liked_users = liked_users;
//	}

	public PostDTO(String username, String blog, Integer blog_id, long liked_users) {
		super();
		this.username = username;
		this.blog = blog;
		this.post_id = blog_id;
		// this.liked_users = liked_users;
	}

	public PostDTO() {
		super();

	}
}

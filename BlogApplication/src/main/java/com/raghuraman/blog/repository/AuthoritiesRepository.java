package com.raghuraman.blog.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raghuraman.blog.entity.Authorities;
import com.raghuraman.blog.entity.User;

@Repository
@Transactional
public interface AuthoritiesRepository extends JpaRepository<Authorities, Integer> {

}

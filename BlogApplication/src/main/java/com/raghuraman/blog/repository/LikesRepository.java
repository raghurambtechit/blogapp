package com.raghuraman.blog.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raghuraman.blog.entity.Comments;
import com.raghuraman.blog.entity.Likes;

@Repository
@Transactional
public interface LikesRepository extends JpaRepository<Likes, Integer> {

	List<Likes> findAllByUsername(String username);

}

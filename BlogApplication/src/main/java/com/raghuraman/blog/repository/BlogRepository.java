package com.raghuraman.blog.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raghuraman.blog.DTO.PostDTO;
import com.raghuraman.blog.entity.Authorities;
import com.raghuraman.blog.entity.Blog;
import com.raghuraman.blog.entity.Comments;

@Repository
@Transactional
public interface BlogRepository extends JpaRepository<Blog, Integer> {

	List<Blog> findAllByUserUsername(String username);

	// public List<Blog> findAllByUser_idUsername(int user_id);

	//	public List<Blog> findAllByUsernameUsername(String username);

	// public List<Comments> findAllByCommentBlog_id(Integer blog_id);

}

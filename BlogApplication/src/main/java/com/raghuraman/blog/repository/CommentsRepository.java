package com.raghuraman.blog.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.raghuraman.blog.entity.Blog;
import com.raghuraman.blog.entity.Comments;

@Repository
@Transactional
public interface CommentsRepository extends JpaRepository<Comments, Integer> {

	List<Comments> findAllByUsername(String username);

}

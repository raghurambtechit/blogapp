package com.raghuraman.blog.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.raghuraman.blog.entity.User;

import java.util.List;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Integer> {

	// public User saveUser(String username, String password, Integer enabled,Integer user_id);

	public User findByUsername(String username);

	public Integer deleteByUsername(String username);

	public User findByUserId(Integer user_id);

}

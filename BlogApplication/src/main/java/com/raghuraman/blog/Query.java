package com.raghuraman.blog;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.raghuraman.blog.DTO.PostDTO;
import com.raghuraman.blog.DTO.UserDTO;
import com.raghuraman.blog.entity.Blog;
import com.raghuraman.blog.entity.Comments;
import com.raghuraman.blog.entity.Likes;
import com.raghuraman.blog.entity.User;

import com.raghuraman.blog.service.Userservice;

@Component
public class Query implements GraphQLQueryResolver {

	@Autowired
	private Userservice userService;

	public String hello() {
		return "Hello  Superops";
	}

	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	public User getuserByName(UserDTO input) {
		return userService.getuserByName(input.getUsername());
	}

	public List<Blog> getUserPosts(String username) {
		return userService.getuserposts(username);
	}

	public List<Comments> getComments(Integer blog_id) {

		return userService.getBlogComments(blog_id);
	}

	public List<Likes> getLikes(Integer blog_id) {

		return userService.getBlogLikes(blog_id);
	}

//	public String getEmployeeName(String empId) {
//		return "TestEmployee for employee id " + empId;
//	}
//	
	// getEmployeeName(empId : String): String

//	public Employee getEmployeeNameIfPresent(InputRequest inputRequest) {
//		Employee emp = new Employee();
//		emp.setEmpId(inputRequest.getEmpId());
//		emp.setEmpName("TestEmployee");
//		return emp;
//	}

//	
//	<!-- 
//

//
//	<!-- https://mvnrepository.com/artifact/com.graphql-java-kickstart/graphql-java-tools -->
//	<dependency>
//	    <groupId>com.graphql-java-kickstart</groupId>
//	    <artifactId>graphql-java-tools</artifactId>
//	    <version>12.0.2</version>
//	</dependency>
//	<!-- https://mvnrepository.com/artifact/com.graphql-java-kickstart/graphql-java-kickstart -->
//	<dependency>
//	    <groupId>com.graphql-java-kickstart</groupId>
//	    <artifactId>graphql-java-kickstart</artifactId>
//	    <version>12.0.0</version>
//	</dependency>
//
//	<!-- https://mvnrepository.com/artifact/com.graphql-java-kickstart/graphql-java-servlet -->
//	<dependency>
//	    <groupId>com.graphql-java-kickstart</groupId>
//	    <artifactId>graphql-java-servlet</artifactId>
//	    <version>12.0.0</version>
//	</dependency>
//	<!-- https://mvnrepository.com/artifact/com.graphql-java-kickstart/graphql-spring-boot-starter -->
//	<dependency>
//	    <groupId>com.graphql-java-kickstart</groupId>
//	    <artifactId>graphql-spring-boot-starter</artifactId>
//	    <version>12.0.0</version>
//	</dependency>
//	<!-- https://mvnrepository.com/artifact/com.graphql-java/graphql-java -->
//	<dependency>
//	    <groupId>com.graphql-java</groupId>
//	    <artifactId>graphql-java</artifactId>
//	    <version>18.0</version>
//	</dependency>

}

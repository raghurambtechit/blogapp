package com.raghuraman.blog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.raghuraman.blog.DTO.CommentDTO;
import com.raghuraman.blog.DTO.LikesDTO;
import com.raghuraman.blog.DTO.PasswordDTO;
import com.raghuraman.blog.DTO.PostDTO;
import com.raghuraman.blog.DTO.UserDTO;
import com.raghuraman.blog.entity.Authorities;
import com.raghuraman.blog.entity.Blog;
import com.raghuraman.blog.entity.Comments;
import com.raghuraman.blog.entity.Likes;
import com.raghuraman.blog.entity.User;
import com.raghuraman.blog.repository.AuthoritiesRepository;
import com.raghuraman.blog.repository.BlogRepository;
import com.raghuraman.blog.repository.CommentsRepository;
import com.raghuraman.blog.repository.LikesRepository;
import com.raghuraman.blog.repository.UserRepository;

@Component
public class Mutation implements GraphQLMutationResolver {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BlogRepository blogRepository;

	@Autowired
	private CommentsRepository commentRepository;

	@Autowired
	private LikesRepository likeRepository;

	@Autowired
	private AuthoritiesRepository authoritiesRepository;

	public User createUser(UserDTO input) {
		try {
			User user = new User(input.getPassword(), input.getEnabled());

			user.setUsername(input.getUsername());
			user.setUser_id(input.getUser_id());
			Authorities authorityTable = new Authorities();
			authorityTable.setAuthority("user");
			authorityTable.setUser_id(input.getUser_id());
			authorityTable.setUsername(input.getUsername());
			user.setAuthorities(authorityTable);
			authorityTable.setUser(user);
			userRepository.save(user);
			return user;
		} catch (Exception ex) {
			System.out.println("Exception" + ex);
			return null;
		}
	}

	public Integer deleteUser(UserDTO input) {
		return userRepository.deleteByUsername(input.getUsername());

	}

	public Blog createBlog(PostDTO input) {
		Blog blog = new Blog(input.getBlog());

		System.out.println(input.getpost_id());

		blog.setPost_id(input.getpost_id());

		User user = userRepository.findByUserId(input.getUser_id());

		blog.setUser(user);

		return blogRepository.save(blog);

	}

	public String deleteBlog(PostDTO input) {

		Blog blog = blogRepository.getOne(input.getpost_id());

		blogRepository.delete(blog);

		return "Blog Deleted";

	}

	public Comments addComment(CommentDTO input, PostDTO input2) {

		Blog blog = blogRepository.getOne(input2.getpost_id());

		Comments comment = new Comments();

		comment.setUsername(input2.getUsername());

		comment.setComment(input.getComment());
		comment.setComment_id(98);

		comment.setBlogForComment(blog);

		return commentRepository.save(comment);

	}

	public String deleteComment(CommentDTO input) {

		Comments comment = commentRepository.getOne(input.getComment_id());

		commentRepository.delete(comment);

		return "comment Deleted";

	}

	public String addLike(PostDTO input) {

		Blog blog = blogRepository.getOne(input.getpost_id());

		Likes like = new Likes();

		// List<Likes> allLikes = new ArrayList<Likes>();

		like.setUsername(input.getUsername());
		like.setBlog(blog);
		like.setLike_id(input.getLike_id());
		// blog.setLike(allLikes);

		likeRepository.save(like);

		return "Dropped a Heart";

	}

	public String unLike(LikesDTO input) {

		Likes like = likeRepository.getOne(input.getLike_id());

		likeRepository.delete(like);

		return "UnLike Done";

	}

	public Blog updateBlog(PostDTO input) {

		Blog blog = blogRepository.getOne(input.getpost_id());

		if (blog.getUser().getUsername().equals(input.getUsername())) {

			blog.setBlog_content(input.getBlog());

			return blogRepository.save(blog);
		}

		else {
			return null;
		}

	}

	public String updatePassword(UserDTO input, PasswordDTO input2) {

		User user = userRepository.getOne(input.getUser_id());

		if (user.getPassword().equals(input2.getPassword())) {

			user.setPassword(input2.getNewPassword());
			userRepository.save(user);

			return "Password Updated";
		}

		return "Error ";

	}

	public String updateUsername(UserDTO input) {

		User user = userRepository.getOne(input.getUser_id());

		Authorities auth = authoritiesRepository.getOne(input.getUser_id());

		List<Likes> likes = likeRepository.findAllByUsername(user.getUsername());

		List<Comments> comments = commentRepository.findAllByUsername(user.getUsername());

		user.setUsername(input.getNewUsername());
		auth.setUsername(input.getNewUsername());

		for (Likes like : likes) {
			like.setUsername(input.getNewUsername());
		}

		for (Comments comment : comments) {
			comment.setUsername(input.getNewUsername());
		}

		userRepository.save(user);
		authoritiesRepository.save(auth);
		likeRepository.saveAll(likes);
		commentRepository.saveAll(comments);

		return "Updated Username Everywhere";
	}

}

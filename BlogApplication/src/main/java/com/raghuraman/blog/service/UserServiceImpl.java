package com.raghuraman.blog.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.raghuraman.blog.DTO.PostDTO;
import com.raghuraman.blog.entity.Authorities;
import com.raghuraman.blog.entity.Blog;
import com.raghuraman.blog.entity.Comments;
import com.raghuraman.blog.entity.Likes;
import com.raghuraman.blog.entity.User;
import com.raghuraman.blog.repository.BlogRepository;
import com.raghuraman.blog.repository.CommentsRepository;
import com.raghuraman.blog.repository.UserRepository;

@Service
@Transactional
public class UserServiceImpl implements Userservice {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private BlogRepository blogRepository;

	@Autowired
	private CommentsRepository commentRepository;

	private Authorities authorities;

	@Override
	public List<User> getAllUsers() {

		return userRepository.findAll();
	}

	@Override
	public User getuserByName(String username) {

		return userRepository.findByUsername(username);
	}

	@Override
	public List<Blog> getuserposts(String username) {

		return (List<Blog>) blogRepository.findAllByUserUsername(username);
	}

	@Override
	public List<Comments> getBlogComments(Integer post_id) {

		Blog blog = blogRepository.getOne(post_id);

		List<Comments> comment = blog.getComment();

		return comment;

	}

	@Override
	public List<Likes> getBlogLikes(Integer post_id) {
		Blog blog = blogRepository.getOne(post_id);

		List<Likes> like = blog.getLike();

		return like;

	}

}
